# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=3

EGIT_REPO_URI="git://anongit.freedesktop.org/~glisse/radeondb"

if [[ ${PV} = 9999* ]]; then
	GIT_ECLASS="git-2"
fi

inherit autotools eutils ${GIT_ECLASS}

DESCRIPTION="a Radeon debugging tool for replaying GPU command stream"

HOMEPAGE="http://jglisse.livejournal.com/"

LICENSE="GPL-3"

SLOT="0"

KEYWORDS="~amd64 ~x86"

IUSE=""

DEPEND="dev-libs/jansson"
RDEPEND="${DEPEND}"

#S="${WORKDIR}/${P}"

src_unpack() {
	[[ $PV = 9999* ]] && git_src_unpack
}

src_prepare() {
	[[ $PV = 9999* ]] && git_src_prepare

	eautoreconf
}

src_configure() {
	econf
}

src_compile() {
	emake || die "emake failed"
}

src_install() {
	emake DESTDIR="${D}" install || die "emake install failed"
}
