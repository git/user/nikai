# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=3

[[ ${PV} = 9999* ]] && GIT="git-2"
EGIT_REPO_URI="git://pinball.git.sourceforge.net/gitroot/pinball/pinball"

inherit games autotools eutils ${GIT}

DESCRIPTION="SDL OpenGL pinball game"
HOMEPAGE="http://pinball.sourceforge.net/"
LICENSE="GPL-2"

SLOT="0"
KEYWORDS="~amd64 ~ppc ~sparc ~x86"
IUSE=""

RDEPEND="virtual/opengl
	x11-libs/libSM
	media-libs/libsdl[opengl,video,X]
	media-libs/sdl-image[png]
	media-libs/sdl-mixer[vorbis]
	>=sys-devel/libtool-2.2.6b"
DEPEND="${RDEPEND}
	x11-libs/libXt"

S=${WORKDIR}/${MY_P}

src_prepare() {
	sed -i -e '/dnl/d' {src,test}/Makefile.am || die #334899
	sed -i -e '/ACLOCAL_AMFLAGS= -I libltdl.m4/d' Makefile.am || die
	sed -i -e '/AM_CONFIG_HEADER.config.h./d' configure.ac || die
	sed -i -e '/AC_CONFIG_AUX_DIR..libltdl.config../d' configure.ac || die
	sed -i -e '/AC_LIBTOOL_DLOPEN/d' configure.ac || die
	sed -i -e '/LT_CONFIG_LTDL_DIR..libltdl../d' configure.ac || die
	sed -i -e '/LT_INIT/d' configure.ac || die
	sed -i -e '/LTDL_INIT/d' configure.ac || die
	sed -i -e '/AC_CONFIG_MACRO_DIR..libltdl..m4../d' configure.ac || die
	sed -i -e '/AC_SUBST.INCLTDL./d' configure.ac || die
	sed -i -e 's/AC_SUBST.LIBLTDL./AC_PROG_RANLIB/' configure.ac || die

	rm -rf libltdl
	eautoreconf
}

src_configure() {
	egamesconf --with-x
}

src_compile() {
	emake CXXFLAGS="${CXXFLAGS}" || die "emake failed"
}

src_install() {
	dodoc README || die "dodoc failed"
	emake DESTDIR="${D}" install || die "emake install failed"
	dosym "${GAMES_BINDIR}"/pinball "${GAMES_BINDIR}"/emilia-pinball
	mv "${D}/${GAMES_PREFIX}/include" "${D}/usr/" \
		|| die "mv failed (include)"
	dodir /usr/bin
	mv "${D}/${GAMES_BINDIR}/pinball-config" "${D}/usr/bin/" \
		|| die "mv failed (bin)"
	sed -i \
		-e 's:-I${prefix}/include/pinball:-I/usr/include/pinball:' \
		"${D}"/usr/bin/pinball-config || die "sed failed"
	newicon data/pinball.xpm ${PN}.xpm
	make_desktop_entry emilia-pinball "Emilia pinball"
	prepgamesdirs
}
